@echo off

docker images | find /c "create-react-app" > temp.txt
set /p CREATE_REACT_APP_COUNT=<temp.txt
del temp.txt

if %CREATE_REACT_APP_COUNT% NEQ 1 (
    docker build -t create-react-app create-react-app/
)

docker run --rm -v %cd%/create-react-app/run.sh:/var/run.sh -v %cd%:/root create-react-app /var/run.sh /root %1

mkdir %1\.devcontainer
copy create-react-app\.devcontainer\* %1\.devcontainer
mkdir %1\.vscode
copy create-react-app\.vscode\* %1\.vscode
