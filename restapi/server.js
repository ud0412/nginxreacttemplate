const express = require('express')
const bodyParser = require('body-parser')
const Pool = require("pg").Pool

const app = express()
app.use(bodyParser.json())

console.log(process.env.POSTGRES_USER)
const pool = new Pool({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT
});

const tableName = "employee";

// Declare a string for the CREATE TABLE SQL statement
const newTableSql = `CREATE TABLE ${tableName} (
    id SERIAL NOT NULL,
    name VARCHAR(255) NOT NULL,
    address TEXT NOT NULL,
    mail VARCHAR(255) NOT NULL,
    phone VARCHAR(20) NOT NULL
);`;

async function createTable() {

    // Promise chain for pg Pool client
    const client = await pool
        .connect()
        .catch(err => {
            console.log('pool .connect ->', err)
        })
    if (client !== undefined) {
        await client.query(newTableSql, (err, res) => {
            // check for errors with client.query()
            if (err) {
                console.log('nCREATE TABLE ->', err)
            }
            if (res) {
                console.log('nCREATE TABLE result:', res)
            }
            
            // Release the pg client instance after last query
            client.release()
            console.log("Client is released")
        })
    }
}

const insertTableSql = `INSERT INTO ${tableName} (
    name, address, mail, phone) `;

async function insertTable(name, address, mail, phone) {

    // Promise chain for pg Pool client
    const client = await pool
        .connect()
        .catch(err => {
            console.log('pool .connect ->', err)
        })
    if (client !== undefined) {
        await client.query(insertTableSql + ` values ('${name}', '${address}', '${mail}', '${phone}');`, (err, res) => {
            // check for errors with client.query()
            if (err) {
                console.log('nINSERT ->', err)
            }
            if (res) {
                console.log('nINSERT result:', res)
            }
            
            // Release the pg client instance after last query
            client.release()
            console.log("Client is released")
        })
    }
}

const selectTableSql = `SELECT * FROM ${tableName} `;

async function selectTable(id, response) {

    // Promise chain for pg Pool client
    const client = await pool
        .connect()
        .catch(err => {
            console.log('pool .connect ->', err)
        })
    if (client !== undefined) {
        await client.query(selectTableSql + ` where id = '${id}';`, (err, res) => {
            // check for errors with client.query()
            if (err) {
                console.log('nSELECT ->', err)
            }
            if (res) {
                console.log('nSELECT result:', res)
            }
            
            // Release the pg client instance after last query
            client.release()
            console.log("Client is released")
            
            response.status(200).json(res.rows)
        })
    }
}

var server = app.listen(process.env.RESTFULAPI_PORT, function() {
    console.log("express server starts")
    createTable()
})

app.get('/employee', function(req, res){
    selectTable(req.query.id, res)
})

app.post('/employee', function(req, res) {
    console.log(req.body)
    
    insertTable(req.body.name, req.body.address, req.body.mail, req.body.phone)
    
    res.end("OK")
})
